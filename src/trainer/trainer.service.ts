import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ITrainer } from 'src/types/trainer';

@Injectable()
export class TrainerService {
  constructor(
    @InjectModel('Trainer')
    private trainerModel: Model<ITrainer>,
  ) {}

  async createTrainer(createTrainerDto): Promise<ITrainer> {
    const newMember = await new this.trainerModel(createTrainerDto);
    return newMember.save();
  }

  async updateTrainer(trainerId: string, updateTrainerDto): Promise<ITrainer> {
    const existingMember = await this.trainerModel.findByIdAndUpdate(
      trainerId,
      updateTrainerDto,
      { new: true },
    );
    if (!existingMember) {
      throw new NotFoundException(`Member #${trainerId} not found`);
    }
    return existingMember;
  }

  async getTrainers(name: string): Promise<ITrainer[]> {
    const filterObj = {};
    if (name) {
      filterObj['name'] = { $regex: name, $options: 'i' };
    }
    const memberData = await this.trainerModel.find(filterObj);
    if (!memberData || memberData.length == 0) {
      throw new NotFoundException('Members data not found!');
    }
    return memberData;
  }

  async getTrainer(trainerId: string): Promise<ITrainer> {
    const existingMember = await this.trainerModel.findById(trainerId).exec();
    if (!existingMember) {
      throw new NotFoundException(`Member #${trainerId} not found`);
    }
    return existingMember;
  }

  async deleteTrainer(trainerId: string): Promise<ITrainer> {
    const deletedMember = await this.trainerModel.findByIdAndDelete(trainerId);
    if (!deletedMember) {
      throw new NotFoundException(`Member #${trainerId} not found`);
    }
    return deletedMember;
  }
}
