import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { TrainerService } from './trainer.service';

@Controller('trainer')
export class TrainerController {
  constructor(private readonly trainerService: TrainerService) {}

  @Post()
  async createTrainer(@Res() response, @Body() createTrainerDto) {
    try {
      const newMember = await this.trainerService.createTrainer(
        createTrainerDto,
      );
      return response.status(HttpStatus.CREATED).json({
        message: 'Trainer has been created successfully',
        newMember,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: err.message?.includes('E11000')
          ? 'มีชื่อ Trainer นี้อยู่แล้ว'
          : err.message,
        error: 'Bad Request',
      });
    }
  }

  @Put('/:id')
  async updateTrainer(
    @Res() response,
    @Param('id') trainerId: string,
    @Body() updateTrainerDto,
  ) {
    try {
      const existingMember = await this.trainerService.updateTrainer(
        trainerId,
        updateTrainerDto,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Trainer has been successfully updated',
        existingMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async getMembers(@Res() response, @Query('name') name: string) {
    try {
      const trainerData = await this.trainerService.getTrainers(name);
      return response.status(HttpStatus.OK).json({
        message: 'All data found successfully',
        trainerData,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:id')
  async getTrainer(@Res() response, @Param('id') trainerId: string) {
    try {
      const existingMember = await this.trainerService.getTrainer(trainerId);
      return response.status(HttpStatus.OK).json({
        message: 'Member found successfully',
        existingMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteTrainer(@Res() response, @Param('id') trainerId: string) {
    try {
      const deletedMember = await this.trainerService.deleteTrainer(trainerId);
      return response.status(HttpStatus.OK).json({
        message: 'Member deleted successfully',
        deletedMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
