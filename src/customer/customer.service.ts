import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ICustomer } from 'src/types/customer';

@Injectable()
export class CustomerService {
  constructor(
    @InjectModel('Customer') private customerModel: Model<ICustomer>,
  ) {}

  async createCustomer(createCustomerDto): Promise<ICustomer> {
    const newCustomer = await new this.customerModel(createCustomerDto);
    return newCustomer.save();
  }

  async updateCustomer(
    customerId: string,
    updateCustomerDto,
  ): Promise<ICustomer> {
    const existingCustomer = await this.customerModel.findByIdAndUpdate(
      customerId,
      updateCustomerDto,
      { new: true },
    );
    if (!existingCustomer) {
      throw new NotFoundException(`Customer #${customerId} not found`);
    }
    return existingCustomer;
  }

  async getCustomers(
    name: string,
    mobileNo: string,
    member_type: string,
  ): Promise<ICustomer[]> {
    const filterObj = {};
    if (name) {
      filterObj['name'] = { $regex: name, $options: 'i' };
    }
    if (mobileNo) {
      filterObj['mobileNo'] = { $regex: mobileNo, $options: 'i' };
    }
    if (member_type) {
      filterObj['member_type'] = member_type;
    }
    const customerData = await this.customerModel.find(filterObj);
    if (!customerData || customerData.length == 0) {
      throw new NotFoundException('Customers data not found!');
    }
    return customerData;
  }

  async getCustomer(customerId: string): Promise<ICustomer> {
    const existingCustomer = await this.customerModel
      .findById(customerId)
      .exec();
    if (!existingCustomer) {
      throw new NotFoundException(`Customer #${customerId} not found`);
    }
    return existingCustomer;
  }

  async deleteCustomer(customerId: string): Promise<ICustomer> {
    const deletedCustomer = await this.customerModel.findByIdAndDelete(
      customerId,
    );
    if (!deletedCustomer) {
      throw new NotFoundException(`Customer #${customerId} not found`);
    }
    return deletedCustomer;
  }
}
