import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { CustomerService } from './customer.service';

@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Post()
  async createCustomer(@Res() response, @Body() body) {
    try {
      const newCustomer = await this.customerService.createCustomer(body);
      return response.status(HttpStatus.CREATED).json({
        message: 'Customer has been created successfully',
        newCustomer,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: err.message?.includes('E11000')
          ? 'มีชื่อ Customer นี้อยู่แล้ว'
          : err.message,
        error: 'Bad Request',
      });
    }
  }

  @Put('/:id')
  async updateCustomer(
    @Res() response,
    @Param('id') customerId: string,
    @Body() updateCustomerDto,
  ) {
    try {
      const existingCustomer = await this.customerService.updateCustomer(
        customerId,
        updateCustomerDto,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Customer has been successfully updated',
        existingCustomer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async getCustomers(
    @Res() response,
    @Query('name') name: string,
    @Query('mobileNo') mobileNo: string,
    @Query('member_type') member_type: string,
  ) {
    try {
      const customerData = await this.customerService.getCustomers(
        name,
        mobileNo,
        member_type,
      );
      return response.status(HttpStatus.OK).json({
        message: 'All customers data found successfully',
        customerData,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:id')
  async getCustomer(@Res() response, @Param('id') customerId: string) {
    try {
      const existingCustomer = await this.customerService.getCustomer(
        customerId,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Customer found successfully',
        existingCustomer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteCustomer(@Res() response, @Param('id') customerId: string) {
    try {
      const deletedCustomer = await this.customerService.deleteCustomer(
        customerId,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Customer deleted successfully',
        deletedCustomer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
