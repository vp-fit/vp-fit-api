import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MemberProgramCheckinSchema } from 'src/models/memberprogramcheckin';
import { MemberProgramSaleSchema } from 'src/models/memberprogramsale';
import { MemberProgramCheckinController } from './member_program_checkin.controller';
import { MemberProgramCheckinService } from './member_program_checkin.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'MemberProgramCheckin', schema: MemberProgramCheckinSchema },
      { name: 'MemberProgramSale', schema: MemberProgramSaleSchema },
    ]),
  ],
  providers: [MemberProgramCheckinService],
  controllers: [MemberProgramCheckinController],
})
export class MemberProgramCheckinModule {}
