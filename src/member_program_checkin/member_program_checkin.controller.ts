import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { MemberProgramCheckinService } from './member_program_checkin.service';

@Controller('member_program_checkin')
export class MemberProgramCheckinController {
  constructor(
    private readonly memberProgramCheckinService: MemberProgramCheckinService,
  ) {}

  @Post()
  async createMemberProgramCheckin(
    @Res() response,
    @Body() createMemberProgramCheckinDto,
  ) {
    try {
      const newMember =
        await this.memberProgramCheckinService.createMemberProgramCheckin(
          createMemberProgramCheckinDto,
        );
      return response.status(HttpStatus.CREATED).json({
        message: 'Member Program has been created successfully',
        newMember,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: err.message?.includes('E11000')
          ? 'มีชื่อ Member Program นี้อยู่แล้ว'
          : err.message,
        error: 'Bad Request',
      });
    }
  }

  @Put('/:id')
  async updateMemberProgramCheckin(
    @Res() response,
    @Param('id') memberProgramCheckinId: string,
    @Body() updateMemberProgramCheckinDto,
  ) {
    try {
      const existingMember =
        await this.memberProgramCheckinService.updateMemberProgramCheckin(
          memberProgramCheckinId,
          updateMemberProgramCheckinDto,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Member Program has been successfully updated',
        existingMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async getMembers(
    @Res() response,
    @Query('name') name: string,
    @Query('member_program_sale_id') member_program_sale_id: string,
    @Query('customer_id') customer_id: string,
    @Query('trainer_id') trainer_id: string,
    @Query('year') year: number,
    @Query('month') month: number,
    @Query('day') day: number,
  ) {
    try {
      const memberData =
        await this.memberProgramCheckinService.getMemberProgramCheckins(
          name,
          member_program_sale_id,
          customer_id,
          trainer_id,
          year,
          month,
          day,
        );
      return response.status(HttpStatus.OK).json({
        message: 'All member programs data found successfully',
        memberData,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/month')
  async getReportsMonth(
    @Res() response,
    @Query('member_program_id') member_program_id: string,
    @Query('customer_id') customer_id: string,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
    @Query('month') month: number,
  ) {
    try {
      const data =
        await this.memberProgramCheckinService.getMemberProgramCheckinsReportMonth(
          member_program_id,
          customer_id,
          sale_id,
          year,
          month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/year')
  async getReportsYear(
    @Res() response,
    @Query('member_program_id') member_program_id: string,
    @Query('customer_id') customer_id: string,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.memberProgramCheckinService.getMemberProgramCheckinsReportYear(
          member_program_id,
          customer_id,
          sale_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/month/export')
  async getReportsMonthExport(
    @Res() response,
    @Query('member_program_id') member_program_id: string,
    @Query('customer_id') customer_id: string,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
    @Query('month') month: number,
  ) {
    try {
      const wb = await this.memberProgramCheckinService
        .getMemberProgramCheckinsReportMonthExcel(
          member_program_id,
          customer_id,
          sale_id,
          year,
          month,
        )
        .catch((err) => {
          throw new HttpException(
            {
              message: err.message,
            },
            HttpStatus.BAD_REQUEST,
          );
        });
      wb.write(`membership_monthly_${year}_${month}.xlsx`, response);
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/year/export')
  async getReportsYearExport(
    @Res() response,
    @Query('member_program_id') member_program_id: string,
    @Query('customer_id') customer_id: string,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
  ) {
    try {
      const wb = await this.memberProgramCheckinService
        .getMemberProgramCheckinsReportYearExcel(
          member_program_id,
          customer_id,
          sale_id,
          year,
        )
        .catch((err) => {
          throw new HttpException(
            {
              message: err.message,
            },
            HttpStatus.BAD_REQUEST,
          );
        });
      wb.write(`membership_yearly_${year}.xlsx`, response);
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:id')
  async getMemberProgramCheckin(
    @Res() response,
    @Param('id') memberProgramCheckinId: string,
  ) {
    try {
      const existingMember =
        await this.memberProgramCheckinService.getMemberProgramCheckin(
          memberProgramCheckinId,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Member found successfully',
        existingMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteMemberProgramCheckin(
    @Res() response,
    @Param('id') memberProgramCheckinId: string,
  ) {
    try {
      const deletedMember =
        await this.memberProgramCheckinService.deleteMemberProgramCheckin(
          memberProgramCheckinId,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Member deleted successfully',
        deletedMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
