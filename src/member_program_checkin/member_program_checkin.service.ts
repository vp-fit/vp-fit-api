import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IMemberProgramCheckin } from 'src/types/memberprogramcheckin';
import { IMemberProgramSale } from 'src/types/memberprogramsale';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const xl = require('excel4node');

@Injectable()
export class MemberProgramCheckinService {
  constructor(
    @InjectModel('MemberProgramCheckin')
    private memberProgramCheckinModel: Model<IMemberProgramCheckin>,
    @InjectModel('MemberProgramSale')
    private memberProgramSaleModel: Model<IMemberProgramSale>,
  ) {}

  async createMemberProgramCheckin(
    createMemberProgramCheckinDto,
  ): Promise<IMemberProgramCheckin> {
    const newMember = await new this.memberProgramCheckinModel(
      createMemberProgramCheckinDto,
    );
    return newMember.save();
  }

  async updateMemberProgramCheckin(
    memberProgramCheckinId: string,
    updateMemberProgramCheckinDto,
  ): Promise<IMemberProgramCheckin> {
    const existingMember =
      await this.memberProgramCheckinModel.findByIdAndUpdate(
        memberProgramCheckinId,
        updateMemberProgramCheckinDto,
        { new: true },
      );
    if (!existingMember) {
      throw new NotFoundException(
        `Member #${memberProgramCheckinId} not found`,
      );
    }
    return existingMember;
  }

  async getMemberProgramCheckins(
    name: string,
    member_program_sale_id: string,
    customer_id: string,
    trainer_id: string,
    year: number,
    month: number,
    day: number,
  ): Promise<IMemberProgramCheckin[]> {
    const filterObj = {};
    if (name) {
      filterObj['name'] = { $regex: name, $options: 'i' };
    }
    if (member_program_sale_id) {
      filterObj['member_program_sale_id'] = member_program_sale_id;
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (trainer_id) {
      filterObj['trainer_id'] = customer_id;
    }
    if (year) {
      filterObj['year'] = year;
    }
    if (month) {
      filterObj['month'] = month;
    }
    if (day) {
      filterObj['day'] = day;
    }
    const memberData = await this.memberProgramCheckinModel.find(filterObj);
    if (!memberData || memberData.length == 0) {
      throw new NotFoundException('Members data not found!');
    }
    return memberData;
  }

  getDaysInMonth(year, month) {
    return new Date(year, month, 0).getDate();
  }

  async getMemberProgramCheckinsReportMonthExcel(
    member_program_id: string,
    customer_id: string,
    sale_id: string,
    year: number,
    month: number,
  ): Promise<any> {
    const filterObj = {};
    if (member_program_id) {
      filterObj['member_program_id'] = member_program_id;
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['year'] = year;
    }
    if (month) {
      filterObj['month'] = month;
    }
    const data = await this.memberProgramCheckinModel.find(filterObj);
    if (!data || data.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const wb = new xl.Workbook();

    const ws = wb.addWorksheet('Sheet 1');

    ws.cell(1, 1)
      .string('Membership Name')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });

    ws.cell(1, 2)
      .string('Customer Name')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });
    const daysInMonth = this.getDaysInMonth(year, month);
    const monthlist = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    for (let i = 1; i <= daysInMonth; i++) {
      ws.cell(1, 2 + i)
        .string(`${i}-${monthlist[month - 1]}-${year}`)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
    }
    const statByProgramSale = {};
    const statByProgramSaleIndex = [];
    for (let i = 0; i < data.length; i++) {
      const checkinData = data[i];
      if (!statByProgramSale[checkinData.member_program_sale_id]) {
        statByProgramSale[checkinData.member_program_sale_id] = {};
        statByProgramSaleIndex.push(checkinData.member_program_sale_id);
      }

      if (
        !statByProgramSale[checkinData.member_program_sale_id][checkinData.day]
      ) {
        statByProgramSale[checkinData.member_program_sale_id][
          checkinData.day
        ] = 0;
      }
      statByProgramSale[checkinData.member_program_sale_id][
        checkinData.day
      ] = 1;
    }

    for (let i = 0; i < statByProgramSaleIndex.length; i++) {
      const saleId = statByProgramSaleIndex[i];
      const statByProgramSaleDate = statByProgramSale[saleId];
      const saleData = await this.memberProgramSaleModel
        .findById(saleId)
        .exec();
      ws.cell(2 + i, 1)
        .string(saleData.member_program_name)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });

      ws.cell(2 + i, 2)
        .string(saleData.customer_name)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
      const daysInMonth = this.getDaysInMonth(year, month);
      for (let j = 1; j <= daysInMonth; j++) {
        ws.cell(2 + i, 2 + j)
          .string(`${statByProgramSaleDate[`${j}`] ? '✅' : ''}`)
          .style({
            alignment: {
              horizontal: 'center',
              vertical: 'center',
            },
          });
      }
    }
    return wb;
  }

  async getMemberProgramCheckinsReportYearExcel(
    member_program_id: string,
    customer_id: string,
    sale_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (member_program_id) {
      filterObj['member_program_id'] = member_program_id;
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['year'] = year;
    }
    const data = await this.memberProgramCheckinModel.find(filterObj);
    if (!data || data.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const wb = new xl.Workbook();

    const ws = wb.addWorksheet('Sheet 1');

    ws.cell(1, 1)
      .string('Membership Name')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });

    ws.cell(1, 2)
      .string('Customer Name')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });
    const monthlist = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    for (let i = 1; i <= 12; i++) {
      ws.cell(1, 2 + i)
        .string(`${monthlist[i - 1]}-${year}`)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
    }
    const statByProgramSale = {};
    const statByProgramSaleIndex = [];
    for (let i = 0; i < data.length; i++) {
      const checkinData = data[i];
      if (!statByProgramSale[checkinData.member_program_sale_id]) {
        statByProgramSale[checkinData.member_program_sale_id] = {};
        statByProgramSaleIndex.push(checkinData.member_program_sale_id);
      }

      if (
        !statByProgramSale[checkinData.member_program_sale_id][
          checkinData.month
        ]
      ) {
        statByProgramSale[checkinData.member_program_sale_id][
          checkinData.month
        ] = 0;
      }
      statByProgramSale[checkinData.member_program_sale_id][
        checkinData.month
      ] += 1;
    }

    for (let i = 0; i < statByProgramSaleIndex.length; i++) {
      const saleId = statByProgramSaleIndex[i];
      const statByProgramSaleDate = statByProgramSale[saleId];
      const saleData = await this.memberProgramSaleModel
        .findById(saleId)
        .exec();
      ws.cell(2 + i, 1)
        .string(saleData.member_program_name)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });

      ws.cell(2 + i, 2)
        .string(saleData.customer_name)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
      for (let j = 1; j <= 12; j++) {
        ws.cell(2 + i, 2 + j)
          .string(
            `${
              statByProgramSaleDate[`${j}`] ? statByProgramSaleDate[`${j}`] : ''
            }`,
          )
          .style({
            alignment: {
              horizontal: 'center',
              vertical: 'center',
            },
          });
      }
    }
    return wb;
  }

  async getMemberProgramCheckinsReportMonth(
    member_program_id: string,
    customer_id: string,
    sale_id: string,
    year: number,
    month: number,
  ): Promise<any> {
    const filterObj = {};
    if (member_program_id) {
      filterObj['member_program_id'] = member_program_id;
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['year'] = year;
    }
    if (month) {
      filterObj['month'] = month;
    }
    const data = await this.memberProgramCheckinModel.find(filterObj);
    if (!data || data.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const statByProgramSale = {};
    const statByProgramSaleIndex = [];
    for (let i = 0; i < data.length; i++) {
      const checkinData = data[i];
      if (!statByProgramSale[checkinData.member_program_sale_id]) {
        statByProgramSale[checkinData.member_program_sale_id] = {};
        statByProgramSaleIndex.push(checkinData.member_program_sale_id);
      }

      if (
        !statByProgramSale[checkinData.member_program_sale_id][checkinData.day]
      ) {
        statByProgramSale[checkinData.member_program_sale_id][
          checkinData.day
        ] = 0;
      }
      statByProgramSale[checkinData.member_program_sale_id][
        checkinData.day
      ] = 1;
    }

    const results = [];
    for (let i = 0; i < statByProgramSaleIndex.length; i++) {
      const saleId = statByProgramSaleIndex[i];
      const statByProgramSaleDate = statByProgramSale[saleId];
      const saleData = await this.memberProgramSaleModel
        .findById(saleId)
        .exec();
      results.push({
        saleData: saleData,
        statByProgramSaleDate: statByProgramSaleDate,
      });
    }
    return results;
  }

  async getMemberProgramCheckinsReportYear(
    member_program_id: string,
    customer_id: string,
    sale_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (member_program_id) {
      filterObj['member_program_id'] = member_program_id;
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['year'] = year;
    }
    const data = await this.memberProgramCheckinModel.find(filterObj);
    if (!data || data.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const statByProgramSale = {};
    const statByProgramSaleIndex = [];
    for (let i = 0; i < data.length; i++) {
      const checkinData = data[i];
      if (!statByProgramSale[checkinData.member_program_sale_id]) {
        statByProgramSale[checkinData.member_program_sale_id] = {};
        statByProgramSaleIndex.push(checkinData.member_program_sale_id);
      }

      if (
        !statByProgramSale[checkinData.member_program_sale_id][
          checkinData.month
        ]
      ) {
        statByProgramSale[checkinData.member_program_sale_id][
          checkinData.month
        ] = 0;
      }
      statByProgramSale[checkinData.member_program_sale_id][
        checkinData.month
      ] += 1;
    }

    const results = [];
    for (let i = 0; i < statByProgramSaleIndex.length; i++) {
      const saleId = statByProgramSaleIndex[i];
      const statByProgramSaleDate = statByProgramSale[saleId];
      const saleData = await this.memberProgramSaleModel
        .findById(saleId)
        .exec();
      results.push({
        saleData: saleData,
        statByProgramSaleDate: statByProgramSaleDate,
      });
    }
    return results;
  }

  async getMemberProgramCheckin(
    memberProgramCheckinId: string,
  ): Promise<IMemberProgramCheckin> {
    const existingMember = await this.memberProgramCheckinModel
      .findById(memberProgramCheckinId)
      .exec();
    if (!existingMember) {
      throw new NotFoundException(
        `Member #${memberProgramCheckinId} not found`,
      );
    }
    return existingMember;
  }

  async deleteMemberProgramCheckin(
    memberProgramCheckinId: string,
  ): Promise<IMemberProgramCheckin> {
    const deletedMember =
      await this.memberProgramCheckinModel.findByIdAndDelete(
        memberProgramCheckinId,
      );
    if (!deletedMember) {
      throw new NotFoundException(
        `Member #${memberProgramCheckinId} not found`,
      );
    }
    return deletedMember;
  }
}
