import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ICustomer } from 'src/types/customer';
import { ISale } from 'src/types/sale';
import { ITrainer } from 'src/types/trainer';
import { ITrainerProgram } from 'src/types/trainerprogram';
import { ITrainerProgramSale } from 'src/types/trainerprogramsale';

@Injectable()
export class TrainerProgramSaleService {
  constructor(
    @InjectModel('TrainerProgramSale')
    private trainerProgramSaleModel: Model<ITrainerProgramSale>,
    @InjectModel('TrainerProgram')
    private trainerProgramModel: Model<ITrainerProgram>,
    @InjectModel('Sale')
    private saleModel: Model<ISale>,
    @InjectModel('Customer')
    private customerModel: Model<ICustomer>,
    @InjectModel('Trainer')
    private trainerModel: Model<ITrainer>,
  ) {}

  async createTrainerProgramSale(
    createTrainerProgramSaleDto,
  ): Promise<ITrainerProgramSale> {
    const newTrainer = await new this.trainerProgramSaleModel(
      createTrainerProgramSaleDto,
    );
    return newTrainer.save();
  }

  async updateTrainerProgramSale(
    trainerProgramSaleId: string,
    updateTrainerProgramSaleDto,
  ): Promise<ITrainerProgramSale> {
    const existingTrainer =
      await this.trainerProgramSaleModel.findByIdAndUpdate(
        trainerProgramSaleId,
        updateTrainerProgramSaleDto,
        { new: true },
      );
    if (!existingTrainer) {
      throw new NotFoundException(`Trainer #${trainerProgramSaleId} not found`);
    }
    return existingTrainer;
  }

  async getTrainerProgramSales(
    name: string,
    customer_id: string,
    trainer_id: string,
  ): Promise<ITrainerProgramSale[]> {
    const filterObj = {};
    if (name) {
      filterObj['name'] = { $regex: name, $options: 'i' };
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (trainer_id) {
      filterObj['trainer_id'] = trainer_id;
    }
    const trainerData = await this.trainerProgramSaleModel.find(filterObj);
    if (!trainerData || trainerData.length == 0) {
      throw new NotFoundException('Trainers data not found!');
    }
    return trainerData;
  }

  async getTrainerProgramSale(
    trainerProgramSaleId: string,
  ): Promise<ITrainerProgramSale> {
    const existingTrainer = await this.trainerProgramSaleModel
      .findById(trainerProgramSaleId)
      .exec();
    if (!existingTrainer) {
      throw new NotFoundException(`Trainer #${trainerProgramSaleId} not found`);
    }
    return existingTrainer;
  }

  async deleteTrainerProgramSale(
    trainerProgramSaleId: string,
  ): Promise<ITrainerProgramSale> {
    const deletedTrainer = await this.trainerProgramSaleModel.findByIdAndDelete(
      trainerProgramSaleId,
    );
    if (!deletedTrainer) {
      throw new NotFoundException(`Trainer #${trainerProgramSaleId} not found`);
    }
    return deletedTrainer;
  }

  async getTrainerProgramSalesReportBySaleMonth(
    sale_id: string,
    start_year: number,
    start_month: number,
    end_year: number,
    end_month: number,
  ): Promise<any> {
    const filterObj = {};
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (start_month && start_year) {
      filterObj['created_on'] = {
        $gte: new Date(start_year, start_month - 1, 1),
        $lt: new Date(end_year, end_month, 1),
      };
    }
    const datas = await this.trainerProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.sale_id]) {
        stat[data.sale_id] = {};
        statIndex.push(data.sale_id);
      }

      if (!stat[data.sale_id][date.getDate()]) {
        stat[data.sale_id][date.getDate()] = 0;
      }
      stat[data.sale_id][date.getDate()] += data.price;
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const saleId = statIndex[i];
      const stats = stat[saleId];
      const data = await this.saleModel.findById(saleId).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getTrainerProgramSalesReportByCustomerMonth(
    customer_id: string,
    start_year: number,
    start_month: number,
    end_year: number,
    end_month: number,
  ): Promise<any> {
    const filterObj = {};
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (start_month && start_year) {
      filterObj['created_on'] = {
        $gte: new Date(start_year, start_month - 1, 1),
        $lt: new Date(end_year, end_month, 1),
      };
    }
    const datas = await this.trainerProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.customer_id]) {
        stat[data.customer_id] = {};
        statIndex.push(data.customer_id);
      }

      if (!stat[data.customer_id][date.getDate()]) {
        stat[data.customer_id][date.getDate()] = 0;
      }
      stat[data.customer_id][date.getDate()] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const Id = statIndex[i];
      const stats = stat[Id];
      const data = await this.customerModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getTrainerProgramSalesReportByTrainerMonth(
    trainer_id: string,
    start_year: number,
    start_month: number,
    end_year: number,
    end_month: number,
  ): Promise<any> {
    const filterObj = {};
    if (trainer_id) {
      filterObj['trainer_id'] = trainer_id;
    }
    if (start_month && start_year) {
      filterObj['created_on'] = {
        $gte: new Date(start_year, start_month - 1, 1),
        $lt: new Date(end_year, end_month, 1),
      };
    }
    const datas = await this.trainerProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.trainer_id]) {
        stat[data.trainer_id] = {};
        statIndex.push(data.trainer_id);
      }

      if (!stat[data.trainer_id][date.getDate()]) {
        stat[data.trainer_id][date.getDate()] = 0;
      }
      stat[data.trainer_id][date.getDate()] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const Id = statIndex[i];
      const stats = stat[Id];
      const data = await this.trainerModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getTrainerProgramSalesReportByTrainerProgramMonth(
    trainer_program_id: string,
    start_year: number,
    start_month: number,
    end_year: number,
    end_month: number,
  ): Promise<any> {
    const filterObj = {};
    if (trainer_program_id) {
      filterObj['trainer_program_id'] = trainer_program_id;
    }
    if (start_month && start_year) {
      filterObj['created_on'] = {
        $gte: new Date(start_year, start_month - 1, 1),
        $lt: new Date(end_year, end_month, 1),
      };
    }
    const datas = await this.trainerProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const statProgram = {};
    const statProgramIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!statProgram[data.trainer_program_id]) {
        statProgram[data.trainer_program_id] = {};
        statProgramIndex.push(data.trainer_program_id);
      }

      if (!statProgram[data.trainer_program_id][date.getDate()]) {
        statProgram[data.trainer_program_id][date.getDate()] = 0;
      }
      statProgram[data.trainer_program_id][date.getDate()] += Number(
        data.price,
      );
    }

    const results = [];
    for (let i = 0; i < statProgramIndex.length; i++) {
      const Id = statProgramIndex[i];
      const stats = statProgram[Id];
      const data = await this.trainerProgramModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getTrainerProgramSalesReportByProgramSaleMonth(
    customer_id: string,
    trainer_program_id: string,
    sale_id: string,
    start_year: number,
    start_month: number,
    end_year: number,
    end_month: number,
  ): Promise<any> {
    const filterObj = {};
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (trainer_program_id) {
      filterObj['trainer_program_id'] = trainer_program_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (start_month && start_year) {
      filterObj['created_on'] = {
        $gte: new Date(start_year, start_month - 1, 1),
        $lt: new Date(end_year, end_month, 1),
      };
    }
    const datas = await this.trainerProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data._id]) {
        stat[data._id] = {};
        statIndex.push(data);
      }

      if (!stat[data._id][date.getDate()]) {
        stat[data._id][date.getDate()] = 0;
      }
      stat[data._id][date.getDate()] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const programSale = statIndex[i];
      const stats = stat[programSale._id];
      results.push({
        data: programSale,
        stats: stats,
      });
    }
    return results;
  }

  async getTrainerProgramSalesReportByCustomerYear(
    customer_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (year) {
      filterObj['created_on'] = {
        $gte: new Date(year, 0, 1),
        $lt: new Date(year + 1, 0, 1),
      };
    }
    const datas = await this.trainerProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.customer_id]) {
        stat[data.customer_id] = {};
        statIndex.push(data.customer_id);
      }

      if (!stat[data.customer_id][date.getMonth() + 1]) {
        stat[data.customer_id][date.getMonth() + 1] = 0;
      }
      stat[data.customer_id][date.getMonth() + 1] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const Id = statIndex[i];
      const stats = stat[Id];
      const data = await this.customerModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getTrainerProgramSalesReportByTrainerYear(
    trainer_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (trainer_id) {
      filterObj['trainer_id'] = trainer_id;
    }
    if (year) {
      filterObj['created_on'] = {
        $gte: new Date(year, 0, 1),
        $lt: new Date(year + 1, 0, 1),
      };
    }
    const datas = await this.trainerProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.trainer_id]) {
        stat[data.trainer_id] = {};
        statIndex.push(data.trainer_id);
      }

      if (!stat[data.trainer_id][date.getMonth() + 1]) {
        stat[data.trainer_id][date.getMonth() + 1] = 0;
      }
      stat[data.trainer_id][date.getMonth() + 1] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const Id = statIndex[i];
      const stats = stat[Id];
      const data = await this.trainerModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getTrainerProgramSalesReportByTrainerProgramYear(
    trainer_program_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (trainer_program_id) {
      filterObj['trainer_program_id'] = trainer_program_id;
    }
    if (year) {
      filterObj['created_on'] = {
        $gte: new Date(year, 0, 1),
        $lt: new Date(year + 1, 0, 1),
      };
    }
    const datas = await this.trainerProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const statProgram = {};
    const statProgramIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!statProgram[data.trainer_program_id]) {
        statProgram[data.trainer_program_id] = {};
        statProgramIndex.push(data.trainer_program_id);
      }

      if (!statProgram[data.trainer_program_id][date.getMonth() + 1]) {
        statProgram[data.trainer_program_id][date.getMonth() + 1] = 0;
      }
      statProgram[data.trainer_program_id][date.getMonth() + 1] += Number(
        data.price,
      );
    }

    const results = [];
    for (let i = 0; i < statProgramIndex.length; i++) {
      const Id = statProgramIndex[i];
      const stats = statProgram[Id];
      const data = await this.trainerProgramModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getTrainerProgramSalesReportBySaleYear(
    sale_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['created_on'] = {
        $gte: new Date(year, 0, 1),
        $lt: new Date(year + 1, 0, 1),
      };
    }
    const datas = await this.trainerProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.sale_id]) {
        stat[data.sale_id] = {};
        statIndex.push(data.sale_id);
      }

      if (!stat[data.sale_id][date.getMonth() + 1]) {
        stat[data.sale_id][date.getMonth() + 1] = 0;
      }
      stat[data.sale_id][date.getMonth() + 1] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const Id = statIndex[i];
      const stats = stat[Id];
      const data = await this.saleModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getTrainerProgramSalesReportByProgramSaleYear(
    customer_id: string,
    trainer_program_id: string,
    sale_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (trainer_program_id) {
      filterObj['trainer_program_id'] = trainer_program_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['created_on'] = {
        $gte: new Date(year, 0, 1),
        $lt: new Date(year + 1, 0, 1),
      };
    }
    const datas = await this.trainerProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data._id]) {
        stat[data._id] = {};
        statIndex.push(data);
      }

      if (!stat[data._id][date.getMonth() + 1]) {
        stat[data._id][date.getMonth() + 1] = 0;
      }
      stat[data._id][date.getMonth() + 1] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const programSale = statIndex[i];
      const stats = stat[programSale._id];
      results.push({
        data: programSale,
        stats: stats,
      });
    }
    return results;
  }
}
