import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CustomerSchema } from 'src/models/customer.schema';
import { SaleSchema } from 'src/models/sale.schema';
import { TrainerSchema } from 'src/models/trainer.schema';
import { TrainerProgramSchema } from 'src/models/trainerprogram.schema';
import { TrainerProgramSaleSchema } from 'src/models/trainerprogramsale.schema';
import { TrainerProgramSaleController } from './trainer_program_sale.controller';
import { TrainerProgramSaleService } from './trainer_program_sale.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'TrainerProgramSale', schema: TrainerProgramSaleSchema },
      { name: 'TrainerProgram', schema: TrainerProgramSchema },
      { name: 'Trainer', schema: TrainerSchema },
      { name: 'Sale', schema: SaleSchema },
      { name: 'Customer', schema: CustomerSchema },
    ]),
  ],
  providers: [TrainerProgramSaleService],
  controllers: [TrainerProgramSaleController],
})
export class TrainerProgramSaleModule {}
