import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { TrainerProgramSaleService } from './trainer_program_sale.service';

@Controller('trainer_program_sale')
export class TrainerProgramSaleController {
  constructor(
    private readonly trainerProgramSaleService: TrainerProgramSaleService,
  ) {}

  @Post()
  async createTrainerProgramSale(
    @Res() response,
    @Body() createTrainerProgramSaleDto,
  ) {
    try {
      const newTrainer =
        await this.trainerProgramSaleService.createTrainerProgramSale(
          createTrainerProgramSaleDto,
        );
      return response.status(HttpStatus.CREATED).json({
        message: 'Trainer Program(PT) has been created successfully',
        newTrainer,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: err.message?.includes('E11000')
          ? 'มีชื่อ Trainer Program นี้อยู่แล้ว'
          : err.message,
        error: 'Bad Request',
      });
    }
  }

  @Put('/:id')
  async updateTrainerProgramSale(
    @Res() response,
    @Param('id') trainerProgramSaleId: string,
    @Body() updateTrainerProgramSaleDto,
  ) {
    try {
      const existingTrainer =
        await this.trainerProgramSaleService.updateTrainerProgramSale(
          trainerProgramSaleId,
          updateTrainerProgramSaleDto,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Trainer Program has been successfully updated',
        existingTrainer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async getTrainers(
    @Res() response,
    @Query('name') name: string,
    @Query('customer_id') customer_id: string,
    @Query('trainer_id') trainer_id: string,
  ) {
    try {
      const list = await this.trainerProgramSaleService.getTrainerProgramSales(
        name,
        customer_id,
        trainer_id,
      );
      return response.status(HttpStatus.OK).json({
        message: 'All trainer programs data found successfully',
        list,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:id')
  async getTrainerProgramSale(
    @Res() response,
    @Param('id') trainerProgramSaleId: string,
  ) {
    try {
      const existingTrainer =
        await this.trainerProgramSaleService.getTrainerProgramSale(
          trainerProgramSaleId,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Trainer found successfully',
        existingTrainer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteTrainerProgramSale(
    @Res() response,
    @Param('id') trainerProgramSaleId: string,
  ) {
    try {
      const deletedTrainer =
        await this.trainerProgramSaleService.deleteTrainerProgramSale(
          trainerProgramSaleId,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Trainer deleted successfully',
        deletedTrainer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/bysale/month')
  async getReportsMonthBySale(
    @Res() response,
    @Query('sale_id') sale_id: string,
    @Query('start_year') start_year: number,
    @Query('start_month') start_month: number,
    @Query('end_year') end_year: number,
    @Query('end_month') end_month: number,
  ) {
    try {
      const data =
        await this.trainerProgramSaleService.getTrainerProgramSalesReportBySaleMonth(
          sale_id,
          start_year,
          start_month,
          end_year,
          end_month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/bycustomer/month')
  async getReportsMonthByCustomer(
    @Res() response,
    @Query('customer_id') customer_id: string,
    @Query('start_year') start_year: number,
    @Query('start_month') start_month: number,
    @Query('end_year') end_year: number,
    @Query('end_month') end_month: number,
  ) {
    try {
      const data =
        await this.trainerProgramSaleService.getTrainerProgramSalesReportByCustomerMonth(
          customer_id,
          start_year,
          start_month,
          end_year,
          end_month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/bytrainer/month')
  async getReportsMonthByTrainer(
    @Res() response,
    @Query('trainer_id') trainer_id: string,
    @Query('start_year') start_year: number,
    @Query('start_month') start_month: number,
    @Query('end_year') end_year: number,
    @Query('end_month') end_month: number,
  ) {
    try {
      const data =
        await this.trainerProgramSaleService.getTrainerProgramSalesReportByTrainerMonth(
          trainer_id,
          start_year,
          start_month,
          end_year,
          end_month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/byprogram/month')
  async getReportsMonthByTrainerProgram(
    @Res() response,
    @Query('trainer_program_id') trainer_program_id: string,
    @Query('start_year') start_year: number,
    @Query('start_month') start_month: number,
    @Query('end_year') end_year: number,
    @Query('end_month') end_month: number,
  ) {
    try {
      const data =
        await this.trainerProgramSaleService.getTrainerProgramSalesReportByTrainerProgramMonth(
          trainer_program_id,
          start_year,
          start_month,
          end_year,
          end_month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/byprogramsale/month')
  async getReportsMonthByTrainerProgramSale(
    @Res() response,
    @Query('customer_id') customer_id: string,
    @Query('trainer_program_id') trainer_program_id: string,
    @Query('sale_id') sale_id: string,
    @Query('start_year') start_year: number,
    @Query('start_month') start_month: number,
    @Query('end_year') end_year: number,
    @Query('end_month') end_month: number,
  ) {
    try {
      const data =
        await this.trainerProgramSaleService.getTrainerProgramSalesReportByProgramSaleMonth(
          customer_id,
          trainer_program_id,
          sale_id,
          start_year,
          start_month,
          end_year,
          end_month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/bysale/year')
  async getReportsYearBySale(
    @Res() response,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.trainerProgramSaleService.getTrainerProgramSalesReportBySaleYear(
          sale_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/bycustomer/year')
  async getReportsYearByCustomer(
    @Res() response,
    @Query('customer_id') customer_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.trainerProgramSaleService.getTrainerProgramSalesReportByCustomerYear(
          customer_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/bytrainer/year')
  async getReportsYearByTrainer(
    @Res() response,
    @Query('trainer_id') trainer_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.trainerProgramSaleService.getTrainerProgramSalesReportByTrainerYear(
          trainer_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/byprogram/year')
  async getReportsYearByTrainerProgram(
    @Res() response,
    @Query('trainer_program_id') trainer_program_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.trainerProgramSaleService.getTrainerProgramSalesReportByTrainerProgramYear(
          trainer_program_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/byprogramsale/year')
  async getReportsYearByTrainerProgramSale(
    @Res() response,
    @Query('customer_id') customer_id: string,
    @Query('trainer_program_id') trainer_program_id: string,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.trainerProgramSaleService.getTrainerProgramSalesReportByProgramSaleYear(
          customer_id,
          trainer_program_id,
          sale_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
