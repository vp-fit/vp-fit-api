import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { MemberProgramService } from './member_program.service';

@Controller('member_program')
export class MemberProgramController {
  constructor(private readonly memberProgramService: MemberProgramService) {}

  @Post()
  async createMemberProgram(@Res() response, @Body() createMemberProgramDto) {
    try {
      const newMember = await this.memberProgramService.createMemberProgram(
        createMemberProgramDto,
      );
      return response.status(HttpStatus.CREATED).json({
        message: 'Member Program has been created successfully',
        newMember,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: err.message?.includes('E11000')
          ? 'มีชื่อ Member Program นี้อยู่แล้ว'
          : err.message,
        error: 'Bad Request',
      });
    }
  }

  @Put('/:id')
  async updateMemberProgram(
    @Res() response,
    @Param('id') memberProgramId: string,
    @Body() updateMemberProgramDto,
  ) {
    try {
      const existingMember =
        await this.memberProgramService.updateMemberProgram(
          memberProgramId,
          updateMemberProgramDto,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Member Program has been successfully updated',
        existingMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async getMembers(@Res() response, @Query('name') name: string) {
    try {
      const memberData = await this.memberProgramService.getMemberPrograms(
        name,
      );
      return response.status(HttpStatus.OK).json({
        message: 'All member programs data found successfully',
        memberData,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:id')
  async getMemberProgram(
    @Res() response,
    @Param('id') memberProgramId: string,
  ) {
    try {
      const existingMember = await this.memberProgramService.getMemberProgram(
        memberProgramId,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Member found successfully',
        existingMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteMemberProgram(
    @Res() response,
    @Param('id') memberProgramId: string,
  ) {
    try {
      const deletedMember = await this.memberProgramService.deleteMemberProgram(
        memberProgramId,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Member deleted successfully',
        deletedMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
