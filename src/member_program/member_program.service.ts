import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IMemberProgram } from 'src/types/memberprogram';

@Injectable()
export class MemberProgramService {
  constructor(
    @InjectModel('MemberProgram')
    private memberProgramModel: Model<IMemberProgram>,
  ) {}

  async createMemberProgram(createMemberProgramDto): Promise<IMemberProgram> {
    const newMember = await new this.memberProgramModel(createMemberProgramDto);
    return newMember.save();
  }

  async updateMemberProgram(
    memberProgramId: string,
    updateMemberProgramDto,
  ): Promise<IMemberProgram> {
    const existingMember = await this.memberProgramModel.findByIdAndUpdate(
      memberProgramId,
      updateMemberProgramDto,
      { new: true },
    );
    if (!existingMember) {
      throw new NotFoundException(`Member #${memberProgramId} not found`);
    }
    return existingMember;
  }

  async getMemberPrograms(name: string): Promise<IMemberProgram[]> {
    const filterObj = {};
    if (name) {
      filterObj['name'] = { $regex: name, $options: 'i' };
    }
    const memberData = await this.memberProgramModel.find(filterObj);
    if (!memberData || memberData.length == 0) {
      throw new NotFoundException('Members data not found!');
    }
    return memberData;
  }

  async getMemberProgram(memberProgramId: string): Promise<IMemberProgram> {
    const existingMember = await this.memberProgramModel
      .findById(memberProgramId)
      .exec();
    if (!existingMember) {
      throw new NotFoundException(`Member #${memberProgramId} not found`);
    }
    return existingMember;
  }

  async deleteMemberProgram(memberProgramId: string): Promise<IMemberProgram> {
    const deletedMember = await this.memberProgramModel.findByIdAndDelete(
      memberProgramId,
    );
    if (!deletedMember) {
      throw new NotFoundException(`Member #${memberProgramId} not found`);
    }
    return deletedMember;
  }
}
