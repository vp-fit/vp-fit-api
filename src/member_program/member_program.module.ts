import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MemberProgramSchema } from 'src/models/memberprogram';
import { MemberProgramController } from './member_program.controller';
import { MemberProgramService } from './member_program.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'MemberProgram', schema: MemberProgramSchema },
    ]),
  ],
  providers: [MemberProgramService],
  controllers: [MemberProgramController],
})
export class MemberProgramModule {}
