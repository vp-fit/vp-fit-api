import { Module } from '@nestjs/common';

import { MongooseModule } from '@nestjs/mongoose/dist/mongoose.module';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { CustomerModule } from './customer/customer.module';
import { TrainerProgramModule } from './trainer_program/trainer_program.module';
import { TrainerProgramSaleModule } from './trainer_program_sale/trainer_program_sale.module';
import { TrainerProgramCheckinModule } from './trainer_program_checkin/trainer_program_checkin.module';
import { MemberProgramModule } from './member_program/member_program.module';
import { MemberProgramSaleModule } from './member_program_sale/member_program_sale.module';
import { MemberProgramCheckinModule } from './member_program_checkin/member_program_checkin.module';
import { TrainerModule } from './trainer/trainer.module';
import { ConfigModule } from '@nestjs/config';
import { SaleModule } from './sale/sale.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGO_URI),
    UserModule,
    AuthModule,
    CustomerModule,
    TrainerProgramModule,
    TrainerProgramSaleModule,
    TrainerProgramCheckinModule,
    MemberProgramModule,
    MemberProgramSaleModule,
    MemberProgramCheckinModule,
    TrainerModule,
    SaleModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
