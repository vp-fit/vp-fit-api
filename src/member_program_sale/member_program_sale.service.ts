import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ICustomer } from 'src/types/customer';
import { IMemberProgram } from 'src/types/memberprogram';
import { IMemberProgramSale } from 'src/types/memberprogramsale';
import { ISale } from 'src/types/sale';

@Injectable()
export class MemberProgramSaleService {
  constructor(
    @InjectModel('MemberProgramSale')
    private memberProgramSaleModel: Model<IMemberProgramSale>,
    @InjectModel('MemberProgram')
    private memberProgramModel: Model<IMemberProgram>,
    @InjectModel('Sale')
    private saleModel: Model<ISale>,
    @InjectModel('Customer')
    private customerModel: Model<ICustomer>,
  ) {}

  async createMemberProgramSale(
    createMemberProgramSaleDto,
  ): Promise<IMemberProgramSale> {
    const newMember = await new this.memberProgramSaleModel(
      createMemberProgramSaleDto,
    );
    return newMember.save();
  }

  async updateMemberProgramSale(
    memberProgramSaleId: string,
    updateMemberProgramSaleDto,
  ): Promise<IMemberProgramSale> {
    const existingMember = await this.memberProgramSaleModel.findByIdAndUpdate(
      memberProgramSaleId,
      updateMemberProgramSaleDto,
      { new: true },
    );
    if (!existingMember) {
      throw new NotFoundException(`Member #${memberProgramSaleId} not found`);
    }
    return existingMember;
  }

  async getMemberProgramSales(
    name: string,
    customer_id: string,
    trainer_id: string,
  ): Promise<IMemberProgramSale[]> {
    const filterObj = {};
    if (name) {
      filterObj['name'] = { $regex: name, $options: 'i' };
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (trainer_id) {
      filterObj['trainer_id'] = trainer_id;
    }
    const memberData = await this.memberProgramSaleModel.find(filterObj);
    if (!memberData || memberData.length == 0) {
      throw new NotFoundException('Members data not found!');
    }
    return memberData;
  }

  async getMemberProgramSale(
    memberProgramSaleId: string,
  ): Promise<IMemberProgramSale> {
    const existingMember = await this.memberProgramSaleModel
      .findById(memberProgramSaleId)
      .exec();
    if (!existingMember) {
      throw new NotFoundException(`Member #${memberProgramSaleId} not found`);
    }
    return existingMember;
  }

  async deleteMemberProgramSale(
    memberProgramSaleId: string,
  ): Promise<IMemberProgramSale> {
    const deletedMember = await this.memberProgramSaleModel.findByIdAndDelete(
      memberProgramSaleId,
    );
    if (!deletedMember) {
      throw new NotFoundException(`Member #${memberProgramSaleId} not found`);
    }
    return deletedMember;
  }

  async getMemberProgramSalesReportBySaleMonth(
    sale_id: string,
    start_year: number,
    start_month: number,
    end_year: number,
    end_month: number,
  ): Promise<any> {
    const filterObj = {};
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (start_month && start_year) {
      filterObj['created_on'] = {
        $gte: new Date(start_year, start_month - 1, 1),
        $lt: new Date(end_year, end_month, 1),
      };
    }
    const datas = await this.memberProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.sale_id]) {
        stat[data.sale_id] = {};
        statIndex.push(data.sale_id);
      }

      if (!stat[data.sale_id][date.getDate()]) {
        stat[data.sale_id][date.getDate()] = 0;
      }
      stat[data.sale_id][date.getDate()] += data.price;
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const saleId = statIndex[i];
      const stats = stat[saleId];
      const data = await this.saleModel.findById(saleId).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getMemberProgramSalesReportByCustomerMonth(
    customer_id: string,
    start_year: number,
    start_month: number,
    end_year: number,
    end_month: number,
  ): Promise<any> {
    const filterObj = {};
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (start_month && start_year) {
      filterObj['created_on'] = {
        $gte: new Date(start_year, start_month - 1, 1),
        $lt: new Date(end_year, end_month, 1),
      };
    }
    const datas = await this.memberProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.customer_id]) {
        stat[data.customer_id] = {};
        statIndex.push(data.customer_id);
      }

      if (!stat[data.customer_id][date.getDate()]) {
        stat[data.customer_id][date.getDate()] = 0;
      }
      stat[data.customer_id][date.getDate()] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const Id = statIndex[i];
      const stats = stat[Id];
      const data = await this.customerModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getMemberProgramSalesReportByMemberProgramMonth(
    member_program_id: string,
    start_year: number,
    start_month: number,
    end_year: number,
    end_month: number,
  ): Promise<any> {
    const filterObj = {};
    if (member_program_id) {
      filterObj['member_program_id'] = member_program_id;
    }
    if (start_month && start_year) {
      filterObj['created_on'] = {
        $gte: new Date(start_year, start_month - 1, 1),
        $lt: new Date(end_year, end_month, 1),
      };
    }
    const datas = await this.memberProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.member_program_id]) {
        stat[data.member_program_id] = {};
        statIndex.push(data.member_program_id);
      }

      if (!stat[data.member_program_id][date.getDate()]) {
        stat[data.member_program_id][date.getDate()] = 0;
      }
      stat[data.member_program_id][date.getDate()] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const Id = statIndex[i];
      const stats = stat[Id];
      const data = await this.memberProgramModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getMemberProgramSalesReportByProgramSaleMonth(
    customer_id: string,
    member_program_id: string,
    sale_id: string,
    start_year: number,
    start_month: number,
    end_year: number,
    end_month: number,
  ): Promise<any> {
    const filterObj = {};
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (member_program_id) {
      filterObj['member_program_id'] = member_program_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (start_month && start_year) {
      filterObj['created_on'] = {
        $gte: new Date(start_year, start_month - 1, 1),
        $lt: new Date(end_year, end_month, 1),
      };
    }
    const datas = await this.memberProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data._id]) {
        stat[data._id] = {};
        statIndex.push(data);
      }

      if (!stat[data._id][date.getDate()]) {
        stat[data._id][date.getDate()] = 0;
      }
      stat[data._id][date.getDate()] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const programSale = statIndex[i];
      const stats = stat[programSale._id];
      results.push({
        data: programSale,
        stats: stats,
      });
    }
    return results;
  }

  async getMemberProgramSalesReportByCustomerYear(
    customer_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (year) {
      filterObj['created_on'] = {
        $gte: new Date(year, 0, 1),
        $lt: new Date(year + 1, 0, 1),
      };
    }
    const datas = await this.memberProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.customer_id]) {
        stat[data.customer_id] = {};
        statIndex.push(data.customer_id);
      }

      if (!stat[data.customer_id][date.getMonth() + 1]) {
        stat[data.customer_id][date.getMonth() + 1] = 0;
      }
      stat[data.customer_id][date.getMonth() + 1] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const Id = statIndex[i];
      const stats = stat[Id];
      const data = await this.customerModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getMemberProgramSalesReportByMemberProgramYear(
    member_program_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (member_program_id) {
      filterObj['member_program_id'] = member_program_id;
    }
    if (year) {
      filterObj['created_on'] = {
        $gte: new Date(year, 0, 1),
        $lt: new Date(year + 1, 0, 1),
      };
    }
    const datas = await this.memberProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.member_program_id]) {
        stat[data.member_program_id] = {};
        statIndex.push(data.member_program_id);
      }

      if (!stat[data.member_program_id][date.getMonth() + 1]) {
        stat[data.member_program_id][date.getMonth() + 1] = 0;
      }
      stat[data.member_program_id][date.getMonth() + 1] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const Id = statIndex[i];
      const stats = stat[Id];
      const data = await this.memberProgramModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getMemberProgramSalesReportBySaleYear(
    sale_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['created_on'] = {
        $gte: new Date(year, 0, 1),
        $lt: new Date(year + 1, 0, 1),
      };
    }
    const datas = await this.memberProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data.sale_id]) {
        stat[data.sale_id] = {};
        statIndex.push(data.sale_id);
      }

      if (!stat[data.sale_id][date.getMonth() + 1]) {
        stat[data.sale_id][date.getMonth() + 1] = 0;
      }
      stat[data.sale_id][date.getMonth() + 1] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const Id = statIndex[i];
      const stats = stat[Id];
      const data = await this.saleModel.findById(Id).exec();
      results.push({
        data: data || {},
        stats: stats,
      });
    }
    return results;
  }

  async getMemberProgramSalesReportByProgramSaleYear(
    customer_id: string,
    member_program_id: string,
    sale_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (member_program_id) {
      filterObj['member_program_id'] = member_program_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['created_on'] = {
        $gte: new Date(year, 0, 1),
        $lt: new Date(year + 1, 0, 1),
      };
    }
    const datas = await this.memberProgramSaleModel.find(filterObj);
    if (!datas || datas.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const stat = {};
    const statIndex = [];
    for (let i = 0; i < datas.length; i++) {
      const data = datas[i];
      const date = data.createdAt;
      if (!stat[data._id]) {
        stat[data._id] = {};
        statIndex.push(data);
      }

      if (!stat[data._id][date.getMonth() + 1]) {
        stat[data._id][date.getMonth() + 1] = 0;
      }
      stat[data._id][date.getMonth() + 1] += Number(data.price);
    }

    const results = [];
    for (let i = 0; i < statIndex.length; i++) {
      const programSale = statIndex[i];
      const stats = stat[programSale._id];
      results.push({
        data: programSale,
        stats: stats,
      });
    }
    return results;
  }
}
