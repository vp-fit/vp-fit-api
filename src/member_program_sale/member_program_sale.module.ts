import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CustomerSchema } from 'src/models/customer.schema';
import { MemberProgramSchema } from 'src/models/memberprogram';
import { MemberProgramSaleSchema } from 'src/models/memberprogramsale';
import { SaleSchema } from 'src/models/sale.schema';
import { MemberProgramSaleController } from './member_program_sale.controller';
import { MemberProgramSaleService } from './member_program_sale.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'MemberProgramSale', schema: MemberProgramSaleSchema },
      { name: 'MemberProgram', schema: MemberProgramSchema },
      { name: 'Sale', schema: SaleSchema },
      { name: 'Customer', schema: CustomerSchema },
    ]),
  ],
  providers: [MemberProgramSaleService],
  controllers: [MemberProgramSaleController],
})
export class MemberProgramSaleModule {}
