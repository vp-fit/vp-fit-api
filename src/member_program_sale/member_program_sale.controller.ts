import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { MemberProgramSaleService } from './member_program_sale.service';

@Controller('member_program_sale')
export class MemberProgramSaleController {
  constructor(
    private readonly memberProgramSaleService: MemberProgramSaleService,
  ) {}

  @Post()
  async createMemberProgramSale(
    @Res() response,
    @Body() createMemberProgramSaleDto,
  ) {
    try {
      const newMember =
        await this.memberProgramSaleService.createMemberProgramSale(
          createMemberProgramSaleDto,
        );
      return response.status(HttpStatus.CREATED).json({
        message: 'Member Program has been created successfully',
        newMember,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: err.message?.includes('E11000')
          ? 'มีชื่อ Member Program นี้อยู่แล้ว'
          : err.message,
        error: 'Bad Request',
      });
    }
  }

  @Put('/:id')
  async updateMemberProgramSale(
    @Res() response,
    @Param('id') memberProgramSaleId: string,
    @Body() updateMemberProgramSaleDto,
  ) {
    try {
      const existingMember =
        await this.memberProgramSaleService.updateMemberProgramSale(
          memberProgramSaleId,
          updateMemberProgramSaleDto,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Member Program has been successfully updated',
        existingMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async getMembers(
    @Res() response,
    @Query('name') name: string,
    @Query('customer_id') customer_id: string,
    @Query('trainer_id') trainer_id: string,
  ) {
    try {
      const list = await this.memberProgramSaleService.getMemberProgramSales(
        name,
        customer_id,
        trainer_id,
      );
      return response.status(HttpStatus.OK).json({
        message: 'All member programs data found successfully',
        list,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:id')
  async getMemberProgramSale(
    @Res() response,
    @Param('id') memberProgramSaleId: string,
  ) {
    try {
      const existingMember =
        await this.memberProgramSaleService.getMemberProgramSale(
          memberProgramSaleId,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Member found successfully',
        existingMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteMemberProgramSale(
    @Res() response,
    @Param('id') memberProgramSaleId: string,
  ) {
    try {
      const deletedMember =
        await this.memberProgramSaleService.deleteMemberProgramSale(
          memberProgramSaleId,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Member deleted successfully',
        deletedMember,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/bysale/month')
  async getReportsMonthBySale(
    @Res() response,
    @Query('sale_id') sale_id: string,
    @Query('start_year') start_year: number,
    @Query('start_month') start_month: number,
    @Query('end_year') end_year: number,
    @Query('end_month') end_month: number,
  ) {
    try {
      const data =
        await this.memberProgramSaleService.getMemberProgramSalesReportBySaleMonth(
          sale_id,
          start_year,
          start_month,
          end_year,
          end_month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/bycustomer/month')
  async getReportsMonthByCustomer(
    @Res() response,
    @Query('customer_id') customer_id: string,
    @Query('start_year') start_year: number,
    @Query('start_month') start_month: number,
    @Query('end_year') end_year: number,
    @Query('end_month') end_month: number,
  ) {
    try {
      const data =
        await this.memberProgramSaleService.getMemberProgramSalesReportByCustomerMonth(
          customer_id,
          start_year,
          start_month,
          end_year,
          end_month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/byprogram/month')
  async getReportsMonthByMemberProgram(
    @Res() response,
    @Query('member_program_id') member_program_id: string,
    @Query('start_year') start_year: number,
    @Query('start_month') start_month: number,
    @Query('end_year') end_year: number,
    @Query('end_month') end_month: number,
  ) {
    try {
      const data =
        await this.memberProgramSaleService.getMemberProgramSalesReportByMemberProgramMonth(
          member_program_id,
          start_year,
          start_month,
          end_year,
          end_month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/byprogramsale/month')
  async getReportsMonthByMemberProgramSale(
    @Res() response,
    @Query('customer_id') customer_id: string,
    @Query('member_program_id') member_program_id: string,
    @Query('sale_id') sale_id: string,
    @Query('start_year') start_year: number,
    @Query('start_month') start_month: number,
    @Query('end_year') end_year: number,
    @Query('end_month') end_month: number,
  ) {
    try {
      const data =
        await this.memberProgramSaleService.getMemberProgramSalesReportByProgramSaleMonth(
          customer_id,
          member_program_id,
          sale_id,
          start_year,
          start_month,
          end_year,
          end_month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/bysale/year')
  async getReportsYearBySale(
    @Res() response,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.memberProgramSaleService.getMemberProgramSalesReportBySaleYear(
          sale_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/bycustomer/year')
  async getReportsYearByCustomer(
    @Res() response,
    @Query('customer_id') customer_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.memberProgramSaleService.getMemberProgramSalesReportByCustomerYear(
          customer_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/byprogram/year')
  async getReportsYearByMemberProgram(
    @Res() response,
    @Query('member_program_id') member_program_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.memberProgramSaleService.getMemberProgramSalesReportByMemberProgramYear(
          member_program_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/byprogramsale/year')
  async getReportsYearByMemberProgramSale(
    @Res() response,
    @Query('customer_id') customer_id: string,
    @Query('member_program_id') member_program_id: string,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.memberProgramSaleService.getMemberProgramSalesReportByProgramSaleYear(
          customer_id,
          member_program_id,
          sale_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
