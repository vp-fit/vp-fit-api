import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ITrainerProgramCheckin } from 'src/types/trainerprogramcheckin';
import { ITrainerProgramSale } from 'src/types/trainerprogramsale';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const xl = require('excel4node');

@Injectable()
export class TrainerProgramCheckinService {
  constructor(
    @InjectModel('TrainerProgramCheckin')
    private trainerProgramCheckinModel: Model<ITrainerProgramCheckin>,
    @InjectModel('TrainerProgramSale')
    private trainerProgramSaleModel: Model<ITrainerProgramSale>,
  ) {}

  async createTrainerProgramCheckin(
    createTrainerProgramCheckinDto,
  ): Promise<ITrainerProgramCheckin> {
    const newTrainer = await new this.trainerProgramCheckinModel(
      createTrainerProgramCheckinDto,
    );

    const programsale = await this.trainerProgramSaleModel.findById(
      createTrainerProgramCheckinDto.trainer_program_sale_id,
    );

    programsale.remaining_times = programsale.remaining_times - 1;

    programsale.save();

    return newTrainer.save();
  }

  async updateTrainerProgramCheckin(
    trainerProgramCheckinId: string,
    updateTrainerProgramCheckinDto,
  ): Promise<ITrainerProgramCheckin> {
    const existingTrainer =
      await this.trainerProgramCheckinModel.findByIdAndUpdate(
        trainerProgramCheckinId,
        updateTrainerProgramCheckinDto,
        { new: true },
      );
    if (!existingTrainer) {
      throw new NotFoundException(
        `Trainer #${trainerProgramCheckinId} not found`,
      );
    }
    return existingTrainer;
  }

  async getTrainerProgramCheckins(
    name: string,
    trainer_program_sale_id: string,
    customer_id: string,
    year: number,
    month: number,
    day: number,
  ): Promise<ITrainerProgramCheckin[]> {
    const filterObj = {};
    if (name) {
      filterObj['name'] = { $regex: name, $options: 'i' };
    }
    if (trainer_program_sale_id) {
      filterObj['trainer_program_sale_id'] = trainer_program_sale_id;
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (year) {
      filterObj['year'] = year;
    }
    if (month) {
      filterObj['month'] = month;
    }
    if (day) {
      filterObj['day'] = day;
    }
    const trainerData = await this.trainerProgramCheckinModel.find(filterObj);
    if (!trainerData || trainerData.length == 0) {
      throw new NotFoundException('Trainers data not found!');
    }
    return trainerData;
  }

  getDaysInMonth(year, month) {
    return new Date(year, month, 0).getDate();
  }

  async getTrainerProgramCheckinsReportMonthExport(
    trainer_program_id: string,
    customer_id: string,
    sale_id: string,
    year: number,
    month: number,
  ): Promise<any> {
    const filterObj = {};
    if (trainer_program_id) {
      filterObj['trainer_program_id'] = trainer_program_id;
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['year'] = year;
    }
    if (month) {
      filterObj['month'] = month;
    }
    const data = await this.trainerProgramCheckinModel.find(filterObj);
    if (!data || data.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const wb = new xl.Workbook();

    const ws = wb.addWorksheet('Sheet 1');

    ws.cell(1, 1)
      .string('Program')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });

    ws.cell(1, 2)
      .string('Customer Name')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });
    ws.cell(1, 3)
      .string('Trainer Name')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });
    ws.cell(1, 4)
      .string('จำนวนครั้งแพ็กเกจ')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });
    ws.cell(1, 5)
      .string('จำนวนครั้งคงเหลือ')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });
    const daysInMonth = this.getDaysInMonth(year, month);
    const monthlist = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    for (let i = 1; i <= daysInMonth; i++) {
      ws.cell(1, 5 + i)
        .string(`${i}-${monthlist[month - 1]}-${year}`)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
    }
    const statByProgramSale = {};
    const statByProgramSaleIndex = [];
    for (let i = 0; i < data.length; i++) {
      const checkinData = data[i];
      if (!statByProgramSale[checkinData.trainer_program_sale_id]) {
        statByProgramSale[checkinData.trainer_program_sale_id] = {};
        statByProgramSaleIndex.push(checkinData.trainer_program_sale_id);
      }

      if (
        !statByProgramSale[checkinData.trainer_program_sale_id][checkinData.day]
      ) {
        statByProgramSale[checkinData.trainer_program_sale_id][
          checkinData.day
        ] = 0;
      }
      statByProgramSale[checkinData.trainer_program_sale_id][
        checkinData.day
      ] = 1;
    }

    for (let i = 0; i < statByProgramSaleIndex.length; i++) {
      const saleId = statByProgramSaleIndex[i];
      const statByProgramSaleDate = statByProgramSale[saleId];
      const saleData = await this.trainerProgramSaleModel
        .findById(saleId)
        .exec();
      ws.cell(2 + i, 1)
        .string(saleData.trainer_program_name)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });

      ws.cell(2 + i, 2)
        .string(saleData.customer_name)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
      ws.cell(2 + i, 3)
        .string(saleData.trainer_name)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
      ws.cell(2 + i, 4)
        .string(`${saleData.times}`)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
      ws.cell(2 + i, 5)
        .string(`${saleData.remaining_times}`)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
      const daysInMonth = this.getDaysInMonth(year, month);
      console.log('statByProgramSaleDate', statByProgramSaleDate);
      for (let j = 1; j <= daysInMonth; j++) {
        ws.cell(2 + i, 5 + j)
          .string(
            `${
              statByProgramSaleDate[`${j}`] ? statByProgramSaleDate[`${j}`] : ''
            }`,
          )
          .style({
            alignment: {
              horizontal: 'center',
              vertical: 'center',
            },
          });
      }
    }
    return wb;
  }

  async getTrainerProgramCheckinsReportYear(
    trainer_program_id: string,
    customer_id: string,
    sale_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (trainer_program_id) {
      filterObj['trainer_program_id'] = trainer_program_id;
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['year'] = year;
    }
    const data = await this.trainerProgramCheckinModel.find(filterObj);
    if (!data || data.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const statByProgramSale = {};
    const statByProgramSaleIndex = [];
    for (let i = 0; i < data.length; i++) {
      const checkinData = data[i];
      if (!statByProgramSale[checkinData.trainer_program_sale_id]) {
        statByProgramSale[checkinData.trainer_program_sale_id] = {};
        statByProgramSaleIndex.push(checkinData.trainer_program_sale_id);
      }

      if (
        !statByProgramSale[checkinData.trainer_program_sale_id][
          checkinData.month
        ]
      ) {
        statByProgramSale[checkinData.trainer_program_sale_id][
          checkinData.month
        ] = 0;
      }
      statByProgramSale[checkinData.trainer_program_sale_id][
        checkinData.month
      ] += 1;
    }

    const results = [];

    for (let i = 0; i < statByProgramSaleIndex.length; i++) {
      const saleId = statByProgramSaleIndex[i];
      const statByProgramSaleDate = statByProgramSale[saleId];
      const saleData = await this.trainerProgramSaleModel
        .findById(saleId)
        .exec();
      results.push({
        saleData: saleData,
        statByProgramSaleDate: statByProgramSaleDate,
      });
    }
    return results;
  }

  async getTrainerProgramCheckinsReportMonth(
    trainer_program_id: string,
    customer_id: string,
    sale_id: string,
    year: number,
    month: number,
  ): Promise<any> {
    const filterObj = {};
    if (trainer_program_id) {
      filterObj['trainer_program_id'] = trainer_program_id;
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['year'] = year;
    }
    if (month) {
      filterObj['month'] = month;
    }
    const data = await this.trainerProgramCheckinModel.find(filterObj);
    if (!data || data.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const statByProgramSale = {};
    const statByProgramSaleIndex = [];
    for (let i = 0; i < data.length; i++) {
      const checkinData = data[i];
      if (!statByProgramSale[checkinData.trainer_program_sale_id]) {
        statByProgramSale[checkinData.trainer_program_sale_id] = {};
        statByProgramSaleIndex.push(checkinData.trainer_program_sale_id);
      }

      if (
        !statByProgramSale[checkinData.trainer_program_sale_id][checkinData.day]
      ) {
        statByProgramSale[checkinData.trainer_program_sale_id][
          checkinData.day
        ] = 0;
      }
      statByProgramSale[checkinData.trainer_program_sale_id][
        checkinData.day
      ] = 1;
    }

    const results = [];

    for (let i = 0; i < statByProgramSaleIndex.length; i++) {
      const saleId = statByProgramSaleIndex[i];
      const statByProgramSaleDate = statByProgramSale[saleId];
      const saleData = await this.trainerProgramSaleModel
        .findById(saleId)
        .exec();
      results.push({
        saleData: saleData,
        statByProgramSaleDate: statByProgramSaleDate,
      });

      // const daysInMonth = this.getDaysInMonth(year, month);
    }
    return results;
  }

  async getTrainerProgramCheckinsReportYearExport(
    trainer_program_id: string,
    customer_id: string,
    sale_id: string,
    year: number,
  ): Promise<any> {
    const filterObj = {};
    if (trainer_program_id) {
      filterObj['trainer_program_id'] = trainer_program_id;
    }
    if (customer_id) {
      filterObj['customer_id'] = customer_id;
    }
    if (sale_id) {
      filterObj['sale_id'] = sale_id;
    }
    if (year) {
      filterObj['year'] = year;
    }
    const data = await this.trainerProgramCheckinModel.find(filterObj);
    if (!data || data.length == 0) {
      throw new NotFoundException('data not found!');
    }

    const wb = new xl.Workbook();

    const ws = wb.addWorksheet('Sheet 1');

    ws.cell(1, 1)
      .string('Program Name')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });

    ws.cell(1, 2)
      .string('Customer Name')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });
    ws.cell(1, 3)
      .string('Trainer Name')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });
    ws.cell(1, 4)
      .string('จำนวนครั้งแพ็กเกจ')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });
    ws.cell(1, 5)
      .string('จำนวนครั้งคงเหลือ')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      });
    const monthlist = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    for (let i = 1; i <= 12; i++) {
      ws.cell(1, 5 + i)
        .string(`${monthlist[i - 1]}-${year}`)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
    }
    const statByProgramSale = {};
    const statByProgramSaleIndex = [];
    for (let i = 0; i < data.length; i++) {
      const checkinData = data[i];
      if (!statByProgramSale[checkinData.trainer_program_sale_id]) {
        statByProgramSale[checkinData.trainer_program_sale_id] = {};
        statByProgramSaleIndex.push(checkinData.trainer_program_sale_id);
      }

      if (
        !statByProgramSale[checkinData.trainer_program_sale_id][
          checkinData.month
        ]
      ) {
        statByProgramSale[checkinData.trainer_program_sale_id][
          checkinData.month
        ] = 0;
      }
      statByProgramSale[checkinData.trainer_program_sale_id][
        checkinData.month
      ] += 1;
    }

    for (let i = 0; i < statByProgramSaleIndex.length; i++) {
      const saleId = statByProgramSaleIndex[i];
      const statByProgramSaleDate = statByProgramSale[saleId];
      const saleData = await this.trainerProgramSaleModel
        .findById(saleId)
        .exec();
      ws.cell(2 + i, 1)
        .string(saleData.trainer_program_name)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });

      ws.cell(2 + i, 2)
        .string(saleData.customer_name)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
      ws.cell(2 + i, 3)
        .string(saleData.trainer_name)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
      ws.cell(2 + i, 4)
        .string(`${saleData.times}`)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
      ws.cell(2 + i, 5)
        .string(`${saleData.remaining_times}`)
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
        });
      for (let j = 1; j <= 12; j++) {
        ws.cell(2 + i, 5 + j)
          .string(
            `${
              statByProgramSaleDate[`${j}`] ? statByProgramSaleDate[`${j}`] : ''
            }`,
          )
          .style({
            alignment: {
              horizontal: 'center',
              vertical: 'center',
            },
          });
      }
    }
    return wb;
  }

  async getTrainerProgramCheckin(
    trainerProgramCheckinId: string,
  ): Promise<ITrainerProgramCheckin> {
    const existingTrainer = await this.trainerProgramCheckinModel
      .findById(trainerProgramCheckinId)
      .exec();
    if (!existingTrainer) {
      throw new NotFoundException(
        `Trainer #${trainerProgramCheckinId} not found`,
      );
    }
    return existingTrainer;
  }

  async deleteTrainerProgramCheckin(
    trainerProgramCheckinId: string,
  ): Promise<ITrainerProgramCheckin> {
    const deletedTrainer =
      await this.trainerProgramCheckinModel.findByIdAndDelete(
        trainerProgramCheckinId,
      );
    if (!deletedTrainer) {
      throw new NotFoundException(
        `Trainer #${trainerProgramCheckinId} not found`,
      );
    }
    return deletedTrainer;
  }
}
