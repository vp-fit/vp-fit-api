import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { TrainerProgramCheckinService } from './trainer_program_checkin.service';

@Controller('trainer_program_checkin')
export class TrainerProgramCheckinController {
  constructor(
    private readonly trainerProgramCheckinService: TrainerProgramCheckinService,
  ) {}

  @Post()
  async createTrainerProgramCheckin(
    @Res() response,
    @Body() createTrainerProgramCheckinDto,
  ) {
    try {
      const newTrainer =
        await this.trainerProgramCheckinService.createTrainerProgramCheckin(
          createTrainerProgramCheckinDto,
        );
      return response.status(HttpStatus.CREATED).json({
        message: 'Trainer Program has been created successfully',
        newTrainer,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: err.message?.includes('E11000')
          ? 'มีชื่อ Trainer Program นี้อยู่แล้ว'
          : err.message,
        error: 'Bad Request',
      });
    }
  }

  @Put('/:id')
  async updateTrainerProgramCheckin(
    @Res() response,
    @Param('id') trainerProgramCheckinId: string,
    @Body() updateTrainerProgramCheckinDto,
  ) {
    try {
      const existingTrainer =
        await this.trainerProgramCheckinService.updateTrainerProgramCheckin(
          trainerProgramCheckinId,
          updateTrainerProgramCheckinDto,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Trainer Program has been successfully updated',
        existingTrainer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async getTrainers(
    @Res() response,
    @Query('name') name: string,
    @Query('trainer_program_sale_id') trainer_program_sale_id: string,
    @Query('customer_id') customer_id: string,
    @Query('year') year: number,
    @Query('month') month: number,
    @Query('day') day: number,
    // year,month,day
  ) {
    try {
      const existing =
        await this.trainerProgramCheckinService.getTrainerProgramCheckins(
          name,
          trainer_program_sale_id,
          customer_id,
          year,
          month,
          day,
        );
      return response.status(HttpStatus.OK).json({
        message: 'All trainer programs checkin data found successfully',
        existing,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/month')
  async getReportsMonth(
    @Res() response,
    @Query('trainer_program_id') trainer_program_id: string,
    @Query('customer_id') customer_id: string,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
    @Query('month') month: number,
  ) {
    try {
      const data =
        await this.trainerProgramCheckinService.getTrainerProgramCheckinsReportMonth(
          trainer_program_id,
          customer_id,
          sale_id,
          year,
          month,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/year')
  async getReportsYear(
    @Res() response,
    @Query('trainer_program_id') trainer_program_id: string,
    @Query('customer_id') customer_id: string,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
  ) {
    try {
      const data =
        await this.trainerProgramCheckinService.getTrainerProgramCheckinsReportYear(
          trainer_program_id,
          customer_id,
          sale_id,
          year,
        );
      return response.status(HttpStatus.OK).json({
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/month/export')
  async getReportsMonthExcel(
    @Res() response,
    @Query('trainer_program_id') trainer_program_id: string,
    @Query('customer_id') customer_id: string,
    @Query('sale_id') sale_id: string,
    @Query('year') year: number,
    @Query('month') month: number,
  ) {
    try {
      const wb = await this.trainerProgramCheckinService
        .getTrainerProgramCheckinsReportMonthExport(
          trainer_program_id,
          customer_id,
          sale_id,
          year,
          month,
        )
        .catch((err) => {
          throw new HttpException(
            {
              message: err.message,
            },
            HttpStatus.BAD_REQUEST,
          );
        });
      wb.write(`PT_monthly_${year}_${month}.xlsx`, response);
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/report/year/export')
  async getReportsYearExcel(
    @Res() response,
    @Query('trainer_program_id') trainer_program_id: string,
    @Query('sale_id') sale_id: string,
    @Query('customer_id') customer_id: string,
    @Query('year') year: number,
  ) {
    try {
      const wb = await this.trainerProgramCheckinService
        .getTrainerProgramCheckinsReportYearExport(
          trainer_program_id,
          customer_id,
          sale_id,
          year,
        )
        .catch((err) => {
          throw new HttpException(
            {
              message: err.message,
            },
            HttpStatus.BAD_REQUEST,
          );
        });
      wb.write(`PT_yearly_${year}.xlsx`, response);
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:id')
  async getTrainerProgramCheckin(
    @Res() response,
    @Param('id') trainerProgramCheckinId: string,
  ) {
    try {
      const existingTrainer =
        await this.trainerProgramCheckinService.getTrainerProgramCheckin(
          trainerProgramCheckinId,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Trainer found successfully',
        existingTrainer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteTrainerProgramCheckin(
    @Res() response,
    @Param('id') trainerProgramCheckinId: string,
  ) {
    try {
      const deletedTrainer =
        await this.trainerProgramCheckinService.deleteTrainerProgramCheckin(
          trainerProgramCheckinId,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Trainer deleted successfully',
        deletedTrainer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
