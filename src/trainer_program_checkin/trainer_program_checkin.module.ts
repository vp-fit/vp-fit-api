import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TrainerProgramCheckinSchema } from 'src/models/trainerprogramcheckin';
import { TrainerProgramSaleSchema } from 'src/models/trainerprogramsale.schema';
import { TrainerProgramCheckinController } from './trainer_program_checkin.controller';
import { TrainerProgramCheckinService } from './trainer_program_checkin.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'TrainerProgramCheckin', schema: TrainerProgramCheckinSchema },
      { name: 'TrainerProgramSale', schema: TrainerProgramSaleSchema },
    ]),
  ],
  providers: [TrainerProgramCheckinService],
  controllers: [TrainerProgramCheckinController],
})
export class TrainerProgramCheckinModule {}
