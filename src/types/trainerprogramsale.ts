import { Document } from 'mongoose';

export interface ITrainerProgramSale extends Document {
  readonly customer_id: string;
  readonly customer_name: string;
  readonly trainer_program_id: string;
  readonly trainer_program_name: string;
  readonly trainer_id: string;
  readonly trainer_name: string;
  readonly sale_id: string;
  readonly sale_name: string;
  readonly price: number;
  readonly times: number;
  remaining_times: number;
  readonly start_date: Date;
  readonly expiry_date: Date;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}
