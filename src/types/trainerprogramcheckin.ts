import { Document } from 'mongoose';

export interface ITrainerProgramCheckin extends Document {
  readonly trainer_program_sale_id: string;

  readonly trainer_program_name: string;

  readonly trainer_id: string;

  readonly trainer_name: string;

  readonly customer_id: string;

  readonly customer_name: string;

  readonly checkin_date: string;

  readonly day: number;

  readonly month: number;

  readonly year: number;

  readonly minute: number;

  readonly hour: number;
}
