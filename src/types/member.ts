import { Document } from 'mongoose';

export interface IMember extends Document {
  readonly name: string;

  readonly address: string;

  readonly contactable_info: string;

  readonly contactable_info_number: string;

  readonly mobileNo: string;

  readonly lineid: string;

  readonly facebook: string;

  readonly member_type: string;

  readonly gender: string;

  readonly note: string;

  readonly expiry_date: Date;
}
