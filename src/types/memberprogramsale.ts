import { Document } from 'mongoose';

export interface IMemberProgramSale extends Document {
  readonly customer_id: string;
  readonly customer_name: string;
  readonly member_program_id: string;
  readonly member_program_name: string;
  readonly sale_id: string;
  readonly sale_name: string;
  readonly price: number;
  readonly start_date: Date;
  readonly expiry_date: Date;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}
