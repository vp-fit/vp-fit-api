import { Document } from 'mongoose';

export interface ISale extends Document {
  readonly name: string;

  readonly mobileNo: string;

  readonly gender: string;

  readonly note: string;

  readonly lineid: string;

  readonly facebook: string;
}
