import { Document } from 'mongoose';

export interface ITrainerProgram extends Document {
  readonly name: string;
  readonly price: number;
  readonly times: number;
}
