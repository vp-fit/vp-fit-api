import { Document } from 'mongoose';

export interface IMemberProgramCheckin extends Document {
  readonly member_program_sale_id: string;

  readonly member_program_name: string;

  readonly customer_id: string;

  readonly checkin_date: string;

  readonly day: number;

  readonly month: number;

  readonly year: number;

  readonly minute: number;

  readonly hour: number;
}
