import { Document } from 'mongoose';

export interface IProgram extends Document {
  readonly name: string;

  readonly price: number;

  readonly address: string;

  readonly contactable_info: string;

  readonly mobileNo: string;

  readonly lineid: string;

  readonly facebook: string;
}
