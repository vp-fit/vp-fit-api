import { Document } from 'mongoose';

export interface ICustomer extends Document {
  readonly name: string;

  readonly address: string;

  readonly mobileNo: string;

  readonly contactable_info: string;

  readonly contactable_info_number: string;

  readonly lineid: string;

  readonly facebook: string;

  readonly member_type: string;

  readonly gender: string;

  readonly note: string;
}
