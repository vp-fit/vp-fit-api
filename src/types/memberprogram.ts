import { Document } from 'mongoose';

export interface IMemberProgram extends Document {
  readonly name: string;
  readonly price: number;
}
