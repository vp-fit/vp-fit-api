import { Document } from 'mongoose';

export interface ICheckin extends Document {
  readonly mid: string;

  readonly checkin_date: string;

  readonly day: number;

  readonly month: number;

  readonly year: number;

  readonly minute: number;

  readonly hour: number;

  readonly current_program: string;
}
