import * as mongoose from 'mongoose';

export const CheckinProgramSchema = new mongoose.Schema(
  {
    program_sell_id: { type: String, required: true },
    checkin_date: { type: Date },
    day: { type: Number },

    month: { type: Number },

    year: { type: Number },

    minute: { type: Number },

    hour: { type: Number },
  },
  {
    timestamps: true,
  },
);
