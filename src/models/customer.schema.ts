import * as mongoose from 'mongoose';

export const CustomerSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, unique: true },
    address: { type: String },
    contactable_info: { type: String },
    contactable_info_number: { type: String },
    mobileNo: { type: String },
    lineid: { type: String },
    facebook: { type: String },
    member_type: { type: String },
    gender: { type: String },
    note: { type: String },
  },
  {
    timestamps: true,
  },
);
