import * as mongoose from 'mongoose';

export const MemberProgramSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    price: { type: Number, required: true },
  },
  {
    timestamps: true,
  },
);
