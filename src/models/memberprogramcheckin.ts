import * as mongoose from 'mongoose';

export const MemberProgramCheckinSchema = new mongoose.Schema(
  {
    member_program_sale_id: { type: String, required: true },

    member_program_id: { type: String, required: true },

    member_program_name: { type: String, required: true },

    customer_id: { type: String, required: true },

    checkin_date: { type: Date },

    day: { type: Number },

    month: { type: Number },

    year: { type: Number },

    minute: { type: Number },

    hour: { type: Number },
  },
  {
    timestamps: true,
  },
);
