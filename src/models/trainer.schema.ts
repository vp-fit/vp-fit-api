import * as mongoose from 'mongoose';

export const TrainerSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, unique: true },
    mobileNo: { type: String },
    gender: { type: String },
    note: { type: String },
    lineid: { type: String },
    facebook: { type: String },
  },
  {
    timestamps: true,
  },
);
