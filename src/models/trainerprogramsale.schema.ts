import * as mongoose from 'mongoose';

export const TrainerProgramSaleSchema = new mongoose.Schema(
  {
    customer_id: { type: String, required: true },
    customer_name: { type: String, required: true },
    trainer_program_id: { type: String, required: true },
    trainer_program_name: { type: String, required: true },
    trainer_id: { type: String, required: true },
    trainer_name: { type: String, required: true },
    sale_id: { type: String },
    sale_name: { type: String },
    price: { type: Number, required: true },
    times: { type: Number, required: true },
    remaining_times: { type: Number, required: true },
    start_date: { type: Date },
    expiry_date: { type: Date },
  },
  {
    timestamps: true,
  },
);
