import * as mongoose from 'mongoose';

export const TrainerProgramCheckinSchema = new mongoose.Schema(
  {
    trainer_program_sale_id: { type: String, required: true },

    trainer_program_name: { type: String, required: true },

    trainer_id: { type: String, required: true },

    trainer_name: { type: String, required: true },

    customer_id: { type: String, required: true },

    customer_name: { type: String, required: true },

    checkin_date: { type: Date },

    day: { type: Number },

    month: { type: Number },

    year: { type: Number },

    minute: { type: Number },

    hour: { type: Number },
  },
  {
    timestamps: true,
  },
);
