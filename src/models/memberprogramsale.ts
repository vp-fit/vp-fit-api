import * as mongoose from 'mongoose';

export const MemberProgramSaleSchema = new mongoose.Schema(
  {
    customer_id: { type: String, required: true },
    customer_name: { type: String, required: true },
    member_program_id: { type: String, required: true },
    member_program_name: { type: String, required: true },
    sale_id: { type: String },
    sale_name: { type: String },
    price: { type: Number },
    start_date: { type: Date },
    expiry_date: { type: Date },
  },
  {
    timestamps: true,
  },
);
