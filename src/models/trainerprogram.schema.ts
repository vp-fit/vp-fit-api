import * as mongoose from 'mongoose';

export const TrainerProgramSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    price: { type: Number, required: true },
    times: { type: Number, required: true },
  },
  {
    timestamps: true,
  },
);
