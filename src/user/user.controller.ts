import { Controller, Get } from '@nestjs/common';
import { UserService } from 'src/user/user.service';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('/getAll')
  async getAll() {
    return this.userService.getAllUser();
  }
}
