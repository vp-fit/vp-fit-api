import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ITrainerProgram } from 'src/types/trainerprogram';

@Injectable()
export class TrainerProgramService {
  constructor(
    @InjectModel('TrainerProgram')
    private trainerProgramModel: Model<ITrainerProgram>,
  ) {}

  async createTrainerProgram(
    createTrainerProgramDto,
  ): Promise<ITrainerProgram> {
    const newTrainer = await new this.trainerProgramModel(
      createTrainerProgramDto,
    );
    return newTrainer.save();
  }

  async updateTrainerProgram(
    trainerProgramId: string,
    updateTrainerProgramDto,
  ): Promise<ITrainerProgram> {
    const existingTrainer = await this.trainerProgramModel.findByIdAndUpdate(
      trainerProgramId,
      updateTrainerProgramDto,
      { new: true },
    );
    if (!existingTrainer) {
      throw new NotFoundException(`Trainer #${trainerProgramId} not found`);
    }
    return existingTrainer;
  }

  async getTrainerPrograms(name: string): Promise<ITrainerProgram[]> {
    const filterObj = {};
    if (name) {
      filterObj['name'] = { $regex: name, $options: 'i' };
    }
    const trainerData = await this.trainerProgramModel.find(filterObj);
    if (!trainerData || trainerData.length == 0) {
      throw new NotFoundException('Trainers data not found!');
    }
    return trainerData;
  }

  async getTrainerProgram(trainerProgramId: string): Promise<ITrainerProgram> {
    const existingTrainer = await this.trainerProgramModel
      .findById(trainerProgramId)
      .exec();
    if (!existingTrainer) {
      throw new NotFoundException(`Trainer #${trainerProgramId} not found`);
    }
    return existingTrainer;
  }

  async deleteTrainerProgram(
    trainerProgramId: string,
  ): Promise<ITrainerProgram> {
    const deletedTrainer = await this.trainerProgramModel.findByIdAndDelete(
      trainerProgramId,
    );
    if (!deletedTrainer) {
      throw new NotFoundException(`Trainer #${trainerProgramId} not found`);
    }
    return deletedTrainer;
  }
}
