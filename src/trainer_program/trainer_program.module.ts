import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TrainerProgramSchema } from 'src/models/trainerprogram.schema';
import { TrainerProgramController } from './trainer_program.controller';
import { TrainerProgramService } from './trainer_program.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'TrainerProgram', schema: TrainerProgramSchema },
    ]),
  ],
  providers: [TrainerProgramService],
  controllers: [TrainerProgramController],
})
export class TrainerProgramModule {}
