import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { TrainerProgramService } from './trainer_program.service';

@Controller('trainer_program')
export class TrainerProgramController {
  constructor(private readonly trainerProgramService: TrainerProgramService) {}

  @Post()
  async createTrainerProgram(@Res() response, @Body() createTrainerProgramDto) {
    try {
      const newTrainer = await this.trainerProgramService.createTrainerProgram(
        createTrainerProgramDto,
      );
      return response.status(HttpStatus.CREATED).json({
        message: 'Trainer Program has been created successfully',
        newTrainer,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: err.message?.includes('E11000')
          ? 'มีชื่อ Trainer Program นี้อยู่แล้ว'
          : err.message,
        error: 'Bad Request',
      });
    }
  }

  @Put('/:id')
  async updateTrainerProgram(
    @Res() response,
    @Param('id') trainerProgramId: string,
    @Body() updateTrainerProgramDto,
  ) {
    try {
      const existingTrainer =
        await this.trainerProgramService.updateTrainerProgram(
          trainerProgramId,
          updateTrainerProgramDto,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Trainer Program has been successfully updated',
        existingTrainer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async getTrainers(@Res() response, @Query('name') name: string) {
    try {
      const trainerData = await this.trainerProgramService.getTrainerPrograms(
        name,
      );
      return response.status(HttpStatus.OK).json({
        message: 'All trainer programs data found successfully',
        trainerData,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:id')
  async getTrainerProgram(
    @Res() response,
    @Param('id') trainerProgramId: string,
  ) {
    try {
      const existingTrainer =
        await this.trainerProgramService.getTrainerProgram(trainerProgramId);
      return response.status(HttpStatus.OK).json({
        message: 'Trainer found successfully',
        existingTrainer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteTrainerProgram(
    @Res() response,
    @Param('id') trainerProgramId: string,
  ) {
    try {
      const deletedTrainer =
        await this.trainerProgramService.deleteTrainerProgram(trainerProgramId);
      return response.status(HttpStatus.OK).json({
        message: 'Trainer deleted successfully',
        deletedTrainer,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
