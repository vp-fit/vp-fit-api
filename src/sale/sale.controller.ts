import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { SaleService } from './sale.service';

@Controller('sale')
export class SaleController {
  constructor(private readonly saleService: SaleService) {}

  @Post()
  async createSale(@Res() response, @Body() createSaleDto) {
    try {
      const newSale = await this.saleService.createSale(createSaleDto);
      return response.status(HttpStatus.CREATED).json({
        message: 'Sale  has been created successfully',
        newSale,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: err.message?.includes('E11000')
          ? 'มีชื่อ Sale  นี้อยู่แล้ว'
          : err.message,
        error: 'Bad Request',
      });
    }
  }

  @Put('/:id')
  async updateSale(
    @Res() response,
    @Param('id') saleId: string,
    @Body() updateSaleDto,
  ) {
    try {
      const existingSale = await this.saleService.updateSale(
        saleId,
        updateSaleDto,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Sale  has been successfully updated',
        existingSale,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async list(@Res() response, @Query('name') name: string) {
    try {
      const list = await this.saleService.getSales(name);
      return response.status(HttpStatus.OK).json({
        message: 'list data found successfully',
        list,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:id')
  async getSale(@Res() response, @Param('id') saleId: string) {
    try {
      const data = await this.saleService.getSale(saleId);
      return response.status(HttpStatus.OK).json({
        message: 'Sale found successfully',
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteSale(@Res() response, @Param('id') saleId: string) {
    try {
      const deletedSale = await this.saleService.deleteSale(saleId);
      return response.status(HttpStatus.OK).json({
        message: 'Sale deleted successfully',
        deletedSale,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
