import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ISale } from 'src/types/sale';

@Injectable()
export class SaleService {
  constructor(
    @InjectModel('Sale')
    private saleModel: Model<ISale>,
  ) {}

  async createSale(createSaleDto): Promise<ISale> {
    const newMember = await new this.saleModel(createSaleDto);
    return newMember.save();
  }

  async updateSale(saleId: string, updateSaleDto): Promise<ISale> {
    const existingMember = await this.saleModel.findByIdAndUpdate(
      saleId,
      updateSaleDto,
      { new: true },
    );
    if (!existingMember) {
      throw new NotFoundException(`Member #${saleId} not found`);
    }
    return existingMember;
  }

  async getSales(name: string): Promise<ISale[]> {
    const filterObj = {};
    if (name) {
      filterObj['name'] = { $regex: name, $options: 'i' };
    }
    const memberData = await this.saleModel.find(filterObj);
    if (!memberData || memberData.length == 0) {
      throw new NotFoundException('Members data not found!');
    }
    return memberData;
  }

  async getSale(saleId: string): Promise<ISale> {
    const existingMember = await this.saleModel.findById(saleId).exec();
    if (!existingMember) {
      throw new NotFoundException(`Member #${saleId} not found`);
    }
    return existingMember;
  }

  async deleteSale(saleId: string): Promise<ISale> {
    const deletedMember = await this.saleModel.findByIdAndDelete(saleId);
    if (!deletedMember) {
      throw new NotFoundException(`Member #${saleId} not found`);
    }
    return deletedMember;
  }
}
