/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-var-requires */
let fs = require('fs');
const data = require('./user.json');
const filedata = [];

for (i = 1; i <= data.length; i++) {
  filedata.push({
    email: 'user' + i + '@vp.com',
    password: '$2b$10$uLaPddRZP5Rczoq15hNWFeWHUs2tzAAcKpzyp6jCBI.4JQn/z8u22',
    ...data[i],
  });
}
fs.writeFile('new-user.json', JSON.stringify(filedata), {}, () => {});
